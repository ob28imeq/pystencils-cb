# Continuous Benchmarking for [Pystenclis](https://i10git.cs.fau.de/pycodegen/pystencils)

Based on [pystenclis-benchmark](https://i10git.cs.fau.de/pycodegen/pystencils-benchmark) and the NHR@FAU Cx Service

The results are uploaded to an InfluxDB and represented with [Grafana](https://www10.cs.fau.de/grafana/d/f96e1306-4fe0-42ef-a17b-53ac04f845a9/pystencils-cpu?orgId=1) 

To add new benchmark:

    1. add a new file to the benchmarks folder
    2. There must be a class in it that file implements the `AbstractBenchmark` interface from [benchmarks/interface.py](pystencils_cb/benchmarks/interface.py)
    3. Register it in the __all__ attribute in the [__init__.py file](pystencils_cb/benchmarks/__init__.py)
