import logging
import re
import os
from pathlib import Path

import pystencils_benchmark as pb
import subprocess

from cbutil.ncu_parser import extract_from_csv
from util import get_parser, set_loglevel, make, get_meta_data, upload_to_influx
import benchmarks

logger = logging.getLogger(__file__)

nvidia_smi_pattern = r"GPU (?P<id>\d+): (?P<name>.+) \(UUID: (?P<UUID>.+)\)"


def query_nvidia_gpus():
    return subprocess.run(['nvidia-smi', '--list-gpus'],
                          text=True,
                          check=True,
                          universal_newlines=True,
                          capture_output=True).stdout


def parse_nvidia_gpus(output):
    for line in output.strip().split("\n"):
        if (match := re.match(nvidia_smi_pattern, line)):
            yield match.groupdict()['id'], match.groupdict()['name'].replace(' ', '')
        else:
            logger.warning(f"{line} does not match nvidia-smi pattern")


def main():
    parser = get_parser()
    args = parser.parse_args()
    set_loglevel(args.loglevel)
    tags = get_meta_data()
    available_gpus = list(parse_nvidia_gpus(query_nvidia_gpus()))

    for compiler in [pb.Compiler.NVCC]:
        for benchmark_type in benchmarks.__all__:
            benchmark = benchmark_type()
            logger.info(f"Running {benchmark.name} compiled with {compiler.name}")
            for dev_id, gpu_name in available_gpus:
                for setup in benchmark.gpu_setups:
                    path = Path(f"./generated/{tags['host']}/{gpu_name}/{compiler.name}/{setup.function_name}")
                    kernel = benchmark.generate_kernel(setup)
                    pb.gpu.generate_benchmark(kernel, path=path, compiler=compiler, timing=True)
                    make(path)
                    ncu_report = f'{path}/report'
                    ncu_args = ['ncu', '-o', ncu_report, '--set=full']
                    ncu_args += [f'{path}/benchmark-{compiler.name}', '1']
                    subprocess.run(ncu_args, check=True, env={**os.environ, 'CUDA_VISIBLE_DEVICES': dev_id})
                    ncu_csv = f'{path}/perf_data.csv'
                    with open(ncu_csv, "w") as csv_file:
                        subprocess.run(['ncu', '-i', f'{ncu_report}.ncu-rep',
                                       '--page=raw', '--csv'], stdout=csv_file, check=True)
                    fields, run_tags = extract_from_csv(ncu_csv)
                    run_tags.update({'compiler': str(compiler)})
                    upload_to_influx(setup.function_name, fields, {**tags, **run_tags,
                                     'array_shape': setup.array_shape}, dry_run=args.dryrun)


if __name__ == '__main__':
    main()
