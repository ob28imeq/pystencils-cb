#!/usr/bin/env python
# coding: utf-8
# from pystencils_benchmark import generate_benchmark, Compiler
import pystencils_benchmark as pb
from pathlib import Path
from util import get_meta_data, process_likwid_files, upload_to_influx, make, execute, get_parser, set_loglevel
import logging
import benchmarks
from plot import generate_plot

logger = logging.getLogger(__file__)


def main():
    parser = get_parser()
    args = parser.parse_args()
    set_loglevel(args.loglevel)

    dry_run = args.dryrun
    likwid = True
    base_tags = get_meta_data()
    performance_group = 'MEM_DP'
    likwid_files = []

    for compiler in [pb.Compiler.GCC]:
        for benchmark_type in benchmarks.__all__:
            benchmark = benchmark_type()
            logger.info(f"Running {benchmark.name} compiled with {compiler.name}")
            for setup in benchmark.cpu_setups:
                path = Path(f"./generated/{base_tags['host']}/{compiler.name}/{setup.function_name}")
                kernel = benchmark.generate_kernel(setup)
                pb.cpu.generate_benchmark(kernel, path=path, timing=False, likwid=likwid)
                make(path)
                execute(path, compiler, likwid)
                likwid_file = f'{path}/likwid_out.json'
                fields = process_likwid_files(likwid_file, performance_group)
                tags = {**base_tags, 'array_shape': setup.array_shape, 'compiler': str(compiler)}
                upload_to_influx(setup.function_name, fields, tags, dry_run=dry_run)
                likwid_files.append(likwid_file)
    try:
        generate_plot(likwid_files, tags["host"])
    except KeyError:
        logging.info(f"Roofline Ploting failed on {tags['host']}")
        pass


if __name__ == '__main__':
    main()
