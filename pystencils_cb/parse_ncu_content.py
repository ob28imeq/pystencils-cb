import numpy as np
import pandas as pd


def parse_ncu_csv(file_name) -> pd.DataFrame:
    return pd.read_csv(file_name, header=[0, 1])


def get_unit(col):
    return col.columns[0]


def detect_prefix(unit):
    if unit[0] == 'G':
        return 1e9
    elif unit[0] == 'M':
        return 1e6
    elif unit[0] == 'K':
        return 1e3
    elif unit[0] == 'm':
        return 1e-3
    elif unit[0] == 'u':
        return 1e-6
    elif unit[0] == 'n':
        return 1e-9
    else:
        return 1


def get_normalized(col):
    unit = get_unit(col)
    if "/" in unit:
        factor = np.divide(*[detect_prefix(u) for u in unit.split("/")])
    else:
        factor = detect_prefix(unit)
    return factor * col


def add_unit_prefix(value, prefix: str):
    return value / detect_prefix(prefix)


def normalize_and_add_prefix(value, prefix: str):
    return add_unit_prefix(get_normalized(value), prefix)


def extract_raw_counter(df: pd.DataFrame):

    fields = pd.DataFrame()
    tags = pd.DataFrame()

    tags["Block Size"] = df["Block Size"]
    tags["Grid Size"] = df["Grid Size"]
    tags["GPU"] = df["device__attribute_display_name"]

    fields["Memory write data volume [GBytes]"] = normalize_and_add_prefix(df["dram__bytes_write.sum"], 'G')
    fields["Memory read data volume [GBytes]"] = normalize_and_add_prefix(df["dram__bytes_read.sum"], 'G')
    fields["Memory data volume [GBytes]"] = fields["Memory write data volume [GBytes]"] + \
        fields["Memory read data volume [GBytes]"]

    fields["Memory write bandwidth [MByte/s]"] = normalize_and_add_prefix(df["dram__bytes_write.sum.per_second"], 'M')
    fields["Memory read bandwidth [MByte/s]"] = normalize_and_add_prefix(df["dram__bytes_read.sum.per_second"], 'M')
    fields["Memory bandwidth [MByte/s]"] = normalize_and_add_prefix(df["dram__bytes.sum.per_second"], 'M')
    fields["Runtime [s]"] = get_normalized(df["gpu__time_duration.sum"])

    fields["SMSP Cycles [Cycles/s]"] = get_normalized(df["smsp__cycles_elapsed.avg.per_second"])
    fields["SMSP Cycles"] = fields["SMSP Cycles [Cycles/s]"] * fields["Runtime [s]"]
    fields["FP inst per cycle"] = 2 * df["smsp__sass_thread_inst_executed_op_dfma_pred_on.sum.per_cycle_elapsed"] + \
        df["smsp__sass_thread_inst_executed_op_dadd_pred_on.sum.per_cycle_elapsed"] + \
        df["smsp__sass_thread_inst_executed_op_dmul_pred_on.sum.per_cycle_elapsed"]
    fields["Total FP inst"] = fields["FP inst per cycle"] * fields["SMSP Cycles"]

    fields["Operational intensity"] = fields["Total FP inst"] / (fields["Memory data volume [GBytes]"] * 1e9)
    fields["P_max [MFlop/s]"] = add_unit_prefix(fields["Operational intensity"]
                                                * fields["Memory bandwidth [MByte/s]"] * 1e6, 'M')
    fields["DP [MFlop/s]"] = np.divide(np.asarray(fields["Total FP inst"]), fields["Runtime [s]"]) / 1e6
    return fields, tags


def tail_to_dict(df):
    return df.tail(1).iloc[0].to_dict()


def extract_from_csv(file_name: str):
    fields, tags = extract_raw_counter(parse_ncu_csv(file_name))
    return tail_to_dict(fields), tail_to_dict(tags)
