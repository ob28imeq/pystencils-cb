import numpy as np
import pystencils as ps
from .interface import AbstractBenchmark, BenchmarkSetup, KernelsType, add_cpu_variations, get_base_gpu_setup


def _generate_kernel(name: str, config: ps.CreateKernelConfig, *, size: int = 40000000) -> KernelsType:
    a, b, c = ps.fields(a=np.ones(size), b=np.ones(
        size), c=np.ones(size))
    assignments = ps.Assignment(a[0], b[0] + c[0])
    return ps.create_kernel(assignments, config=config, function_name=name)


class Vadd(AbstractBenchmark):
    def __init__(self):
        name = "vadd"
        base_setup = BenchmarkSetup(name, ps.CreateKernelConfig(ps.Target.CPU), array_shape=(40_000_000, ))
        super().__init__(name=name, cpu_setups=add_cpu_variations(
            base_setup), gpu_setups=[get_base_gpu_setup(base_setup)])

    def generate_kernel(self, setup: BenchmarkSetup):
        return _generate_kernel(setup.function_name, setup.config, size=setup.array_shape)
