from typing import List, Union, Tuple, Iterable
from types import MappingProxyType
from abc import ABC, abstractmethod
from dataclasses import dataclass, field
import pystencils as ps
from copy import copy
from pystencils.astnodes import KernelFunction

KernelsType = Union[KernelFunction, List[KernelFunction]]


@dataclass
class BenchmarkSetup:
    """The function name as string """
    function_name: str
    """
    The `pystencils.CreateKernelConfig` used for the `pystencils.create_kernel` function
    """
    config: ps.CreateKernelConfig
    """
    The Shape of the used array(s)
    """
    array_shape: Tuple[int]
    """
    Other configuration used for the `Benchmark.generate_kernel` function
    """
    other_config: MappingProxyType = field(default_factory=lambda: MappingProxyType({}))


@dataclass
class AbstractBenchmark(ABC):
    """Abstract base class for generating `pystencils.KernelFunctions` """
    name: str
    cpu_setups: List[BenchmarkSetup]
    gpu_setups: List[BenchmarkSetup]

    @abstractmethod
    def generate_kernel(self, setup: BenchmarkSetup) -> KernelsType:
        """
           Generate kernel function(s) based on the provided configuration.

           Args:
               setup (BenchmarkSetup): The setup for the benchmark.

           Returns:
               KernelsType: A single kernel function or a list of kernel functions.
           """
        pass


def get_omp_setup(base_setup: BenchmarkSetup) -> BenchmarkSetup:
    """ Returns a Benchmark setup base on base_setup that has OpenMP enabled. """
    new_config = copy(base_setup.config)
    new_config.cpu_openmp = True
    return BenchmarkSetup(function_name=f"{base_setup.function_name}_omp",
                          config=new_config,
                          array_shape=base_setup.array_shape,
                          other_config=base_setup.other_config)


def get_vector_setup(base_setup: BenchmarkSetup) -> BenchmarkSetup:
    """ Returns a Benchmark setup base on base_setup that has vectorization enabled. """
    new_config = copy(base_setup.config)
    new_config.cpu_vectorize_info = {'instruction_set': 'best'}
    return BenchmarkSetup(function_name=f"{base_setup.function_name}_vector",
                          config=new_config,
                          array_shape=base_setup.array_shape,
                          other_config=base_setup.other_config)


def get_base_gpu_setup(base_setup: BenchmarkSetup) -> BenchmarkSetup:
    """ Returns a Benchmark setup base on base_setup that has GPU as a target. """
    new_config = copy(base_setup.config)
    new_config.target = ps.Target.GPU
    new_config.backend = ps.Backend.CUDA
    return BenchmarkSetup(function_name=f"{base_setup.function_name}_gpu",
                          config=new_config,
                          array_shape=base_setup.array_shape,
                          other_config=base_setup.other_config)


def add_cpu_variations(base_setup: BenchmarkSetup) -> Iterable[BenchmarkSetup]:
    """ Based on a base setup it returns a list of this benchmark also using OpenMP and vectorization."""
    return [base_setup,
            get_omp_setup(base_setup),
            get_vector_setup(base_setup), ]


def map_add_cpu_variations(base_setups: Iterable[BenchmarkSetup]) -> Iterable[BenchmarkSetup]:
    for base_setup in base_setups:
        yield from add_cpu_variations(base_setup)
