import numpy as np
import pystencils as ps
from .interface import AbstractBenchmark, BenchmarkSetup, KernelsType, add_cpu_variations, get_base_gpu_setup


def _generate_kernel(name: str, config: ps.CreateKernelConfig, *, size: int = (20000, 20000)) -> KernelsType:
    a, b = ps.fields(a=np.ones(size), b=np.ones(
        size))
    assignments = ps.Assignment(a[0, 0], (b[1, 0] + b[0, 1] + b[-1, 0] + b[0, -1]) * 0.25)
    return ps.create_kernel(assignments, config=config, function_name=name)


class Jacobi2D(AbstractBenchmark):
    def __init__(self):
        name = "jacobi2D"
        base_setup = BenchmarkSetup(name, ps.CreateKernelConfig(ps.Target.CPU), array_shape=(20_000, 20_000))
        super().__init__(name=name, cpu_setups=add_cpu_variations(
            base_setup), gpu_setups=[get_base_gpu_setup(base_setup)])

    def generate_kernel(self, setup: BenchmarkSetup):
        return _generate_kernel(setup.function_name, setup.config, size=setup.array_shape)
