import numpy as np
import pystencils as ps
from pystencils import fields, Field
from pystencils.field import create_numpy_array_with_layout

from .interface import AbstractBenchmark, BenchmarkSetup, KernelsType, map_add_cpu_variations

from lbmpy import LBStencil, Stencil, LBMConfig, LBMOptimisation, Method, create_lb_update_rule


def _generate_kernel(name: str, config: ps.CreateKernelConfig, *, size=(32, 32, 32), fixed_shape=True) -> KernelsType:
    ghost_layers = 1
    dtype = np.float64
    stencil = LBStencil(Stencil.D3Q19)

    layout_tuple = (3, 2, 1, 0)
    alignment = False
    byte_offset = 8
    shape = (size[0] + 2 * ghost_layers,
             size[1] + 2 * ghost_layers,
             size[2] + 2 * ghost_layers,
             stencil.Q)

    src_array = create_numpy_array_with_layout(layout=layout_tuple, alignment=alignment,
                                               byte_offset=byte_offset, shape=shape, dtype=dtype)
    src_array.fill(0)

    dst_array = create_numpy_array_with_layout(layout=layout_tuple, alignment=alignment,
                                               byte_offset=byte_offset, shape=shape, dtype=dtype)
    dst_array.fill(0)

    if fixed_shape:
        src = Field.create_from_numpy_array("src", src_array, index_dimensions=1)
        dst = Field.create_from_numpy_array("dst", src_array, index_dimensions=1)
    else:
        src = fields(f'src({stencil.Q}): double[3d]')
        dst = fields(f'dst({stencil.Q}): double[3d]')

    lbm_config = LBMConfig(stencil=stencil, method=Method.SRT,
                           relaxation_rate=1.8, compressible=True)

    lbm_optimisation = LBMOptimisation(symbolic_field=src, symbolic_temporary_field=dst)
    update = create_lb_update_rule(lbm_config=lbm_config,
                                   lbm_optimisation=lbm_optimisation)

    return ps.create_kernel(update, config=config, function_name=name)


class LBMTest(AbstractBenchmark):
    def __init__(self):
        base_pointer = BenchmarkSetup("lbmtest_base_pointer",
                                      ps.CreateKernelConfig(ps.Target.CPU,
                                                            base_pointer_specification=[
                                                                ['spatialInner0'], ['spatialInner1']]),
                                      array_shape=(256, 256, 256),
                                      other_config={'fixed_shape': True})
        no_base_pointer = BenchmarkSetup("lbmtest_base_pointer",
                                         ps.CreateKernelConfig(ps.Target.CPU, base_pointer_specification=[]),
                                         array_shape=(256, 256, 256),
                                         other_config={'fixed_shape': True})
        cpu_setups = [*map_add_cpu_variations([base_pointer, no_base_pointer])]
        super().__init__(name="lbmtest", cpu_setups=cpu_setups, gpu_setups=[])

    def generate_kernel(self, setup: BenchmarkSetup):
        return _generate_kernel(setup.function_name, setup.config, size=setup.array_shape, **setup.other_config)
