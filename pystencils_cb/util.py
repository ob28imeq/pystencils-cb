import pystencils as ps
import re
import platform
import os
import subprocess
from pathlib import Path
from pystencils_benchmark import Compiler
import cbutil as cb
import argparse
import logging
from cbutil.likwid_parser import parse_likwid_json
from cbutil.util import get_time_stamp_from_env


def get_pystencils_commit():
    """Extracts the commit hash of the currently used pystencils version"""
    return ps._version.get_versions()['full-revisionid']


def get_repository_info():
    ret = {}
    project_id_key = "PYSTENCILS_PROJECT_ID"
    branch_key = "PYSTENCILS_BRANCH"
    if (project_id := os.environ.get(project_id_key)):
        ret[project_id_key] = project_id
    if (branch_name := os.environ.get(branch_key)):
        ret[branch_key] = branch_name
    return ret


def get_meta_data():
    ret = {'pystencils-commit': get_pystencils_commit(),
           'host': platform.node(),
           **get_repository_info()}
    try:
        ret.update(cb.util.get_git_infos_from_env())
    except KeyError:
        pass
    return ret


def process_likwid_files(file: str, performance_group: str) -> dict:
    sum_keys = ['AVX DP [MFLOP/s] STAT',
                'CPI STAT',
                'DP [MFLOP/s] STAT',
                'Energy DRAM [J] STAT',
                'Energy [J] STAT',
                'Memory bandwidth [MBytes/s] STAT',
                'Memory data volume [GBytes] STAT',
                'Memory read bandwidth [MBytes/s] STAT',
                'Memory read data volume [GBytes] STAT',
                'Memory write bandwidth [MBytes/s] STAT',
                'Memory write data volume [GBytes] STAT',
                'Operational intensity STAT',
                'Packed [MUOPS/s] STAT',
                'Power DRAM [W] STAT',
                'Power [W] STAT',
                'Scalar [MUOPS/s] STAT']
    avg_keys = ['Clock [MHz] STAT', ]
    max_keys = ['Runtime (RDTSC) [s] STAT', ]
    try:
        data = parse_likwid_json(file, performance_group,
                                 sum_keys=sum_keys,
                                 avg_keys=avg_keys,
                                 min_keys=[],
                                 max_keys=max_keys)
        return {
            re.sub(r"STAT (Avg|Min|Max|Sum)", "", key).strip(): value
            for key, value in data.items()
        }

    except KeyError:
        data = cb.json2dict(file)[performance_group][performance_group]['Metric']
        keys = set([*sum_keys, *avg_keys, *max_keys])
        ret = {}
        for key in keys:
            try:
                clean_key = key.replace('STAT', '').strip()
                ret.update({clean_key: data[clean_key]['Values'][0]})
            except KeyError:
                pass
        return ret


def upload_to_influx(measurement, fields, tags, *, dry_run=False):
    dp = cb.DataPoint(
        measurement=measurement,
        tags=tags,
        fields=fields,
        time=get_time_stamp_from_env()
    )
    up = cb.Uploader()
    up.upload([dp], dry_run=dry_run)


def make(path: Path):
    subprocess.run(['make', '-C', f'{path}'], check=True)


def execute(path: Path, compiler: Compiler, likwid: bool, *, likwid_output_name: str = "likwid_out"):
    args = [f'{path}/benchmark-{compiler.name}', '100']
    if likwid:
        cores = os.cpu_count()
        args = ['likwid-perfctr',
                '-o', f'{path}/{likwid_output_name}.json',
                '-C', f'N:0-{cores-1}',
                '-g', 'MEM_DP',
                '-m',
                *args]
    subprocess.run(args, check=True)


def get_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-d',
        '--dryrun',
        help='proccess but do not upload data, default=false',
        action='store_true',
        default=False,
    )
    parser.add_argument(
        '-log',
        '--loglevel',
        default='warning',
        help=('Provide logging level. '
              'Example --loglevel debug, default=warning')
    )

    return parser


def set_loglevel(loglevel: str):
    logging.basicConfig(level=loglevel.upper())
