import cbutil as cb
from pathlib import Path
import plotting.roofline as rl
import logging
from typing import List, Tuple
from datetime import datetime
from cbutil.util import get_time_stamp_from_env
from util import get_pystencils_commit

logger = logging.getLogger(__file__)


def get_perf_data(likwid_json_file: str):
    '''Extracts OI and perf from a likwid MEM_DP json '''
    data = cb.json2dict(likwid_json_file)
    try:
        base_keys = ['MEM_DP', 'MEM_DP', 'Metric STAT']
        mem_dp = cb.get_from_nested_dict(data, base_keys)
        oi = mem_dp['Operational intensity STAT']['Sum']
        p = mem_dp['DP [MFLOP/s] STAT']['Sum'] * 1e6
    except KeyError:
        base_keys = ['MEM_DP', 'MEM_DP', 'Metric']
        mem_dp = cb.get_from_nested_dict(data, base_keys)
        oi = mem_dp['Operational intensity']['Values'][0]
        p = mem_dp['DP [MFLOP/s]']['Values'][0] * 1e6

    return oi, p


def get_title(host):
    commit = get_pystencils_commit()
    time = datetime.fromtimestamp(get_time_stamp_from_env())
    return f'Roofline Plot for commit {commit} at {time} on {host}'


def split_path(path: str) -> Tuple[str, str]:
    subdirs = Path(path).parts
    return subdirs[2], subdirs[3]


def build_name(compiler: str, benchmark: str) -> str:
    return f"{compiler} {benchmark}"


def get_name(path: str) -> str:
    return build_name(*split_path(path))


def generate_plot(likwid_json_files: List[str], host):

    title = get_title(host)
    fig = rl.create_plot_by_host(host, title=title)
    for likwid_json_file in likwid_json_files:
        try:
            oi, perf = get_perf_data(likwid_json_file)
        except KeyError:
            logger.warning(
                f"Invalid Likwid output file. Ignoring {likwid_json_file}!")
            continue
        name = get_name(likwid_json_file)
        rl.add_data_point(fig,
                          oi,
                          perf,
                          name=name,)
    fig.write_html(f'roofline_{str(get_time_stamp_from_env())}_{host}.html')
